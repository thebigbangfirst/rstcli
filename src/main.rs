use chrono::{DateTime, Utc};
use csv::Writer;
use s3::bucket::Bucket;
use s3::credentials::Credentials;
use s3::error::S3Error;
use s3::region::Region;
use serde::{Deserialize, Serialize};
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::process::Command;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "csp_service")]
struct Opt {
    // A flag, true if used in the command line. Note doc comment will
    // be used for the help message of the flag. The name of the
    // argument will be, by default, based on the name of the field.
    /// Upload covid19 data
    #[structopt(short, long)]
    upload_data: bool,
    /// Store all pacman packages
    #[structopt(short, long)]
    store_pacman_packages: bool,
}

async fn store_pacman_packages() -> Result<(), Box<dyn std::error::Error>> {
    let pacman = Command::new("pacman")
        .arg("-Qet")
        .output()
        .expect("failed to execute process");

    let mut file = File::create("pacman.txt")?;
    file.write_all(pacman.stdout.as_slice())?;

    upload_file_to_s3(
        "earth",
        "sars-cov2",
        Region::UsEast2,
        "pacman.txt".to_string(),
        "pacman.txt.".to_string(),
    )
    .await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    if opt.upload_data {
        upload_data().await?;
    }
    if opt.store_pacman_packages {
        store_pacman_packages().await?;
    }
    Ok(())
}

async fn upload_data() -> Result<(), Box<dyn std::error::Error>> {
    let cases = get_cases().await?;
    write_to_csv(cases).await?;
    upload_file_to_s3("earth", "sars-cov2", Region::UsEast2, get_filename(), format!("sars-cov2-{}.csv", get_current_date())).await?;
    Ok(())
}

#[derive(Deserialize, Serialize, Debug)]
struct CountryInfo {
    id: Option<u32>,
    iso2: Option<String>,
    iso3: Option<String>,
    lat: f64,
    long: f64,
    flag: String,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct CaseInfo {
    country: String,
    country_info: CountryInfo,
    cases: u32,
    today_cases: u32,
    recovered: u32,
    active: u32,
    critical: u32,
    tests: u32,
    continent: String,
}

#[derive(Debug, Serialize)]
struct CaseRepresentation {
    country: String,
    country_iso2: Option<String>,
    cases: u32,
    today_cases: u32,
    recovered: u32,
    active: u32,
    critical: u32,
    tests: u32,
    continent: String,
}

impl CaseInfo {
    fn row_reporesentation(&self) -> CaseRepresentation {
        CaseRepresentation {
            country: self.country.clone(),
            country_iso2: self.country_info.iso2.clone(),
            cases: self.cases,
            today_cases: self.today_cases,
            recovered: self.recovered,
            active: self.active,
            critical: self.critical,
            tests: self.tests,
            continent: self.continent.clone(),
        }
    }
}

pub static URL: &str = "https://corona.lmao.ninja/v2/countries?yesterday=true&sort=cases";

fn get_current_date() -> String {
    let now: DateTime<Utc> = Utc::now();
    format!("{}", now.format("%Y-%m-%d"))
}

fn get_filename() -> String {
    if !Path::new("/tmp/csv/").exists() {
        fs::create_dir("/tmp/csv").unwrap();
    }
    format!("/tmp/csv/sars-cov2-{}.csv", get_current_date())
}

async fn get_cases() -> Result<Vec<CaseInfo>, Box<dyn std::error::Error>> {
    Ok(reqwest::get(URL).await?.json::<Vec<CaseInfo>>().await?)
}

async fn write_to_csv(cases: Vec<CaseInfo>) -> Result<(), Box<dyn std::error::Error>> {
    let mut wrt = Writer::from_path(get_filename())?;
    cases.iter().for_each(|x| {
        wrt.serialize(x.row_reporesentation()).unwrap();
    });
    Ok(())
}

async fn upload_file_to_s3<'a>(
    config_name: &'a str,
    bucket_name: &'a str,
    region: Region,
    filename_from: String,
    filename_to: String,
) -> Result<(), S3Error> {
    let credentials = Credentials::new(None, None, None, Some(config_name.into())).await?;
    let bucket = Bucket::new(bucket_name, region, credentials)?;
    let code = bucket
        .put_object_stream(filename_from, filename_to)
        .await?;
    println!("{}", code);
    Ok(())
}
