FROM rust:1.42
WORKDIR /usr/src/rstcli

COPY . .

RUN cargo build --release

RUN cargo install --path .
